package com.pruebas.mongo.servicio;

import com.pruebas.mongo.modelo.Producto;

import java.util.List;

public interface RepositorioProductoPersonalizado {
    public List<Producto> buscarPorRangoPreciosPersonalizado(double min, double max);

    public void ajustarPrecioProducto(String id, double precio);
}