package com.pruebas.mongo.servicio;

import com.pruebas.mongo.modelo.Producto;

import java.util.List;

public interface ServicioProducto {
    public void agregar(Producto p);
    public Producto buscarPorId(String id);
    public List<Producto> obtenerTodos();
    public void guardarProductoPorId(String idProducto, Producto p);
    public List<Producto> buscarPorRangoPrecio(double minPrecio, double maxPrecio);
    public List<Producto> buscarPorProveedor(String nombreProveedor);

    public void ajustarPrecioProductoPorId(String idProducto, double precio);
}