package com.pruebas.mongo.servicio.impl;


import com.pruebas.mongo.modelo.Producto;
import com.pruebas.mongo.servicio.RepositorioProducto;
import com.pruebas.mongo.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ServicioProductoImpl implements ServicioProducto {

    @Autowired
    RepositorioProducto repositorioProducto;

    @Override
    public void agregar(Producto p) {
        p.setId(null);
        this.repositorioProducto.insert(p);
    }

    @Override
    public Producto buscarPorId(String id) {
        Optional<Producto> r = this.repositorioProducto.findById(id);
        return r.isPresent() ? r.get() : null;
    }

    @Override
    public List<Producto> obtenerTodos() {
        return this.repositorioProducto.findAll();
    }

    @Override
    public void guardarProductoPorId(String idProducto, Producto p) {
        p.setId(idProducto);
        this.repositorioProducto.save(p);
    }

    public List<Producto> buscarPorRangoPrecio(double minPrecio, double maxPrecio) {
        System.out.println(String.format("====== buscarPorRangoPrecio(%f, %f)", minPrecio, maxPrecio));
        return this.repositorioProducto.buscarPorRangoPreciosPersonalizado(minPrecio, maxPrecio);
    }

    @Override
    public List<Producto> buscarPorProveedor(String nombreProveedor) {
        System.out.println(String.format("====== buscarPorProveedor('%s')", nombreProveedor));
        return this.repositorioProducto.buscarPorProveedor(nombreProveedor);
    }

    @Override
    public void ajustarPrecioProductoPorId(String idProducto, double precio) {
        this.repositorioProducto.ajustarPrecioProducto(idProducto, precio);
    }
}
