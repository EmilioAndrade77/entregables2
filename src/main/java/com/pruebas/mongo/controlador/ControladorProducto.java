package com.pruebas.mongo.controlador;

import com.pruebas.mongo.modelo.Producto;
import com.pruebas.mongo.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.lang.invoke.SerializedLambda;
import java.util.List;

@RestController
@RequestMapping("/almacen/v2/productos")
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    // GET []
    @GetMapping
    public List<Producto> obtenerProductos(@RequestParam(required = false) Double maxPrecio,
                                           @RequestParam(required = false) String nombreProveedor) {
        System.out.println(String.format("==== maxPrecio = %f, nombreProveedor = '%s'", maxPrecio, nombreProveedor));

        if(nombreProveedor != null)
            return this.servicioProducto.buscarPorProveedor(nombreProveedor);

        if(maxPrecio != null)
            return this.servicioProducto.buscarPorRangoPrecio(0.0, maxPrecio.doubleValue());

        return this.servicioProducto.obtenerTodos();

    }

    // GET {id}
    @GetMapping("/{id}")
    public Producto obtenerProductoPorId(@PathVariable String id) {
        return this.servicioProducto.buscarPorId(id);
    }

    // POST
    @PostMapping
    public void guardarProducto(@RequestBody Producto p) {
        p.setId(null);
        this.servicioProducto.agregar(p);
    }

    // PUT
    @PutMapping("/{id}")
    public void reemplazarProducto(@PathVariable String id, @RequestBody Producto p) {
        final Producto o = this.servicioProducto.buscarPorId(id);
        if(o == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        this.servicioProducto.guardarProductoPorId(id, p);
    }

    public static class PrecioProducto {
        public double precio;
    }

    @PatchMapping("/{id}")
    public void ajustarPrecio(@PathVariable String id, @RequestBody PrecioProducto pp) {
        this.servicioProducto.ajustarPrecioProductoPorId(id, pp.precio);
    }
}